<?php

use App\Http\Controllers\AuthorController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookController;
use App\Http\Controllers\PatronController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::prefix('v1')->group(function () {

    Route::middleware('throttle:10,1')->group(function () {

        // Books CRUD

        Route::post('/books', [BookController::class, 'store']);
        Route::get('/books', [BookController::class, 'index']);
        Route::get('/books/{id}', [BookController::class, 'show']);
        Route::put('/books/{id}', [BookController::class, 'update']);
        Route::delete('/books/{id}', [BookController::class, 'destroy']);

        Route::get('/booksearch', [BookController::class, 'search']);
        Route::get('/books/by-author/{author_id}', [BookController::class, 'getBooksByAuthor']);


        // Authors CRUD

        Route::post('/authors', [AuthorController::class, 'store']);
        Route::get('/authors', [AuthorController::class, 'index']);
        Route::get('/authors/{id}', [AuthorController::class, 'show']);
        Route::put('/authors/{id}', [AuthorController::class, 'update']);
        Route::delete('/authors/{id}', [AuthorController::class, 'destroy']);


        //  Patron CRUD

        Route::post('/patrons', [PatronController::class, 'store']);
        Route::get('/patrons', [PatronController::class, 'index']);
        Route::get('/patrons/{id}', [PatronController::class, 'show']);
        Route::put('/patrons/{id}', [PatronController::class, 'update']);
        Route::delete('/patrons/{id}', [PatronController::class, 'destroy']);

        // Borrowed Book
        Route::post('/patrons/{patron_id}/borrow/{book_id}', [PatronController::class, 'borrowBook']);

        // Return Borrowed book
        Route::post('/patrons/{patron_id}/return/{book_id}', [PatronController::class, 'returnBook']);


    });
});

