<?php

namespace App\Http\Controllers;

use App\Models\Author;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $authors = Author::all();
            return response()->json($authors);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Unable to fetch authors.'], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'first_name' => 'required',
                'last_name' => 'required',
            ]);
            
            $author = Author::create($request->all());

            return response()->json($author, 201);
        } catch (ValidationException $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Unable to create author.'], 500);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {
            $author = Author::findOrFail($id);
            return response()->json($author);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Author not found.'], 404);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Unable to fetch author.'], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {



        try {
            $request->validate([
                'first_name' => 'required',
                'last_name' => 'required',
            ]);

            $author = Author::findOrFail($id);
            $author->update($request->all());

            return response()->json($author, 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Author not found.'], 404);
        } catch (ValidationException $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Unable to update author.'], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $author = Author::findOrFail($id);
            $author->delete();

            return response()->json(['success' => 'Author Deleted.'], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Author not found.'], 404);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Unable to delete author.'], 500);
        }
    }
}
