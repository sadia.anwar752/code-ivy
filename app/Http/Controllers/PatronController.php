<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Patron;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;

class PatronController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $patrons = Patron::all();
            return response()->json($patrons);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Unable to fetch patrons.'], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'name' => 'required',
                'email' => 'required|email|unique:patrons',
            ]);

            $patron = Patron::create($request->all());
            return response()->json($patron, 201);
        } catch (ValidationException $e) {
            return response()->json(['error' => $e->errors()], 400);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Unable to create patron.'], 500);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {
            $patron = Patron::findOrFail($id);
            return response()->json($patron);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Patron not found.'], 404);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Unable to fetch patron.'], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        try {
            $request->validate([
                'name' => 'required',
                'email' => 'required|email|unique:patrons,email,'.$id,
            ]);

            $patron = Patron::findOrFail($id);
            $patron->update($request->all());

            return response()->json($patron, 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Patron not found.'], 404);
        } catch (ValidationException $e) {
            return response()->json(['error' => $e->errors()], 400);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Unable to update patron.'], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $patron = Patron::findOrFail($id);
            $patron->delete();
            return response()->json(null, 204);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Patron not found.'], 404);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Unable to delete patron.'], 500);
        }
    }

    /**
     * Borrow a book for a patron.
     */
    public function borrowBook(Request $request, $patron_id, $book_id)
    {
        try {
            $patron = Patron::findOrFail($patron_id);
            $book = Book::findOrFail($book_id);

            // Calculate the due date (e.g., 14 days from now)
            $dueDate = now()->addDays(14);

            // Attach the book to the patron and track borrowing details
            $patron->borrowedBooks()->attach($book_id, [
                'borrowed_at' => now(),
                'due_at' => $dueDate,
            ]);

            return response()->json(['message' => 'Book borrowed successfully', 'due_date' => $dueDate]);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Patron or book not found.'], 404);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Unable to borrow book.'], 500);
        }
    }

    /**
     * Return a borrowed book for a patron.
     */
    public function returnBook(Request $request, $patron_id, $book_id)
    {
        try {
            // Find the patron
            $patron = Patron::findOrFail($patron_id);

            // Detach the book from the patron and update borrowing details
            $patron->borrowedBooks()->updateExistingPivot($book_id, [
                'returned_at' => now()
            ]);

            return response()->json(['message' => 'Book returned successfully']);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Patron not found.'], 404);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Unable to return book.'], 500);
        }
    }
}
