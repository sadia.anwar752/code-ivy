<?php

namespace App\Http\Controllers;

use App\Models\Book as Book;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Cache;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $books = Book::all();
            return response()->json($books);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Unable to fetch books.'], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {



        try {
            $request->validate([
                'title' => 'required',
                'description' => 'required',
                'ISBN' => 'required|unique:books',
                'publication_date' => 'required|date',
            ]);

            $book = Book::create($request->all());
            return response()->json($book, 200);
        } catch (ValidationException $e) {
            return response()->json(['error' => $e->errors()], 400);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Unable to create book.'], 500);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {
            $book = Book::findOrFail($id);
            return response()->json($book);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Book not found.'], 404);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Unable to fetch book.'], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        try {
            $request->validate([
                'title' => 'required',
                'description' => 'required',
                'ISBN' => 'required|unique:books,isbn,'.$id,
                'publication_date' => 'required|date',
            ]);

            $book = Book::findOrFail($id);
            $book->update($request->all());

            return response()->json($book, 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Book not found.'], 404);
        } catch (ValidationException $e) {
            return response()->json(['error' => $e->errors()], 400);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Unable to update book.'], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $book = Book::findOrFail($id);
            $book->delete();
            return response()->json(['success' => 'Book Deleted.'], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Book not found.'], 404);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Unable to delete book.'], 500);
        }
    }


    public function search(Request $request)
    { 
        $searchQuery = $request->input('query');
        
        // Generate a unique cache key based on the search query
        $cacheKey = 'search_' . md5($searchQuery);

        // Check if the result is cached
        if (Cache::has($cacheKey)) {
            // Data is cached
            $cachedBooks = true;
            $books = Cache::get($cacheKey);
            $message = 'Data retrieved from cache.';
        } else {
            // Data is not cached
            $cachedBooks = false;
            // If not cached, perform the query and cache the result
            try {
                $books = Book::where('title', 'like', "%$searchQuery%")
                    ->orWhereHas('authors', function ($query) use ($searchQuery) {
                        $query->where('first_name', 'like', "%$searchQuery%")
                            ->orWhere('last_name', 'like', "%$searchQuery%");
                    })
                    ->get();

                // Cache the result for 1 hour
                Cache::put($cacheKey, $books, 3600);

                $message = 'Data retrieved from database and cached.';
            } catch (\Exception $e) {
                return response()->json(['error' => 'Unable to perform search.'], 500);
            }
        }

        return response()->json([
            'books' => $books,
            'cached_books' => $cachedBooks, // Indicates whether the data is cached
            'message' => $message, // Message to indicate whether data is cached or not
        ]);
    }
    public function getBooksByAuthor($author_id)
    {
        try {
            $author = \App\Models\Author::findOrFail($author_id);
            return response()->json($author->books);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Author not found.'], 404);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Unable to fetch books by author.'], 500);
        }
    }
}
