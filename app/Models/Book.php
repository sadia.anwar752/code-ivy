<?php

namespace App\Models;

use App\Models\Author;
use App\Models\Patron;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Book extends Model
{
    use HasFactory;
    protected $fillable = ['title', 'description', 'ISBN', 'publication_date'];

    public function authors()
    {
        return $this->belongsToMany(Author::class);
    }

    public function borrowedBy()
    {
        return $this->belongsToMany(Patron::class, 'borrowed_books')
            ->withPivot('borrowed_at', 'due_at', 'returned_at')
            ->withTimestamps();
    }
}
