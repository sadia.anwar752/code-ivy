<?php

namespace App\Models;

use App\Models\Book as Book;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patron extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'email'];

    public function borrowedBooks()
    {
        return $this->belongsToMany(Book::class, 'borrowed_books')
            ->withPivot('borrowed_at', 'due_at', 'returned_at')
            ->withTimestamps();
    }
}
