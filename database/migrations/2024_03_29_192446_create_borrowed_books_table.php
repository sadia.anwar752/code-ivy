<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        if (!Schema::hasTable('borrowed_books')) {

            Schema::create('borrowed_books', function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('patron_id');
                $table->unsignedBigInteger('book_id');
                $table->timestamp('borrowed_at')->nullable();
                $table->timestamp('due_at')->nullable();
                $table->timestamp('returned_at')->nullable();
                $table->timestamps();

                $table->foreign('patron_id')->references('id')->on('patrons')->onDelete('cascade');
                $table->foreign('book_id')->references('id')->on('books')->onDelete('cascade');
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('borrowed_books');
    }
};
