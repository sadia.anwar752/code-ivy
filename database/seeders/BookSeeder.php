<?php

namespace Database\Seeders;

use App\Models\Book;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Book::create([
            'title' => 'Women, Households, and the Hereafter in the Qur’an',
            'description' => 'The first book in the Harry Potter series.',
            'ISBN' => '978-0-19-889727-9',
            'publication_date' => '1997-06-26',
        ]);
    }
}
