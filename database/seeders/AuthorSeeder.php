<?php

namespace Database\Seeders;

use App\Models\Author;
use App\Models\Book;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AuthorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $author = Author::create([
            'first_name' => 'Feras',
            'last_name' => 'Hamza',
        ]);

        $book = Book::where('title', 'Women, Households, and the Hereafter in the Qur’an')->first();
        $book->authors()->attach($author);
    }
}
